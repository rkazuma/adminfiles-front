import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { FileService } from 'src/app/services/files.service';
import { Files } from '../commons/models/files.model';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  constructor(private fileService: FileService) { }

  fileForm = new FormGroup({
    dateFile: new FormControl('', [Validators.required]),
    nameId: new FormControl('', [Validators.required])
  });

  filesRes: Files;
  newFilesRes: Files;
  errorserver = false;
  notfoundserver = false;
  model: any;
  selectedDevice: any;
  goodResponse = [];
  newgoodResponse = [];

  ngOnInit() {
    this.goHealth();
  }

  goHealth() {
    console.log('Ask component to service health');
    this.fileService.health().subscribe(data => {
      console.log("PUT Request is successful ", data);
    }, error => {
      this.errorserver = true;
      console.log("Error", error);
    });
  }

  onChange(newValue) {
    console.log(newValue);
    this.selectedDevice = newValue;
    this.resetAlert();
  }

  findFile(formDate: string, formId: string, nutId: string) {
    this.clearTable();
    this.resetAlert();
    if (this.selectedDevice == '1') {
      console.log('Consult by Date. ' + formDate);
      this.getFileByDate(formDate);
    } else if (this.selectedDevice == '2') {
      console.log('Consult by Id. ' + formId);
      this.getFileById(formId);
    } else if (this.selectedDevice == '3') {
      console.log('Consult by nut. ' + nutId);
      this.getFileById(nutId);
    }
  }

  getFileByDate(varDate: string) {
    console.log('ask someone if getFileByDate work');
    this.fileService.getFileByDate(varDate).subscribe((data: Files) => {
      this.filesRes = data;
      let evilResponseProps = Object.keys(this.filesRes);
      for (let prop of evilResponseProps) {
        this.goodResponse.push(this.filesRes[prop]);
      }
      console.log(this.goodResponse[0]);
      if(this.goodResponse[0] == null){
        this.notfoundserver = true;
      }
    }, error => {
      this.errorserver = true;
      console.log('Error', error);
    });
  }

  getFileById(varId: string) {
    console.log('ask someone if getFileById work');
    this.fileService.getFileById(varId).subscribe((data: Files) => {
      this.filesRes = data;
      let evilResponseProps = Object.keys(this.filesRes);
      for (let prop of evilResponseProps) {
        this.goodResponse.push(this.filesRes[prop]);
      }
      console.log(this.goodResponse[0]);
      if(this.goodResponse[0] == null){
        this.notfoundserver = true;
      }
    }, error => {
      this.errorserver = true;
      console.log('Error', error);
    });
  }

  getFiles() {
    console.log('ask someone if getFiles work');
    this.fileService.getFiles().subscribe((data: Files) => {
      this.filesRes = data;
      let evilResponseProps = Object.keys(this.filesRes);
      for (let prop of evilResponseProps) {
        this.goodResponse.push(this.filesRes[prop]);
      }
      console.log(this.goodResponse[0]);
    }, error => {
      this.errorserver = true;
      console.log('Error', error);
    });
  }

  // Get 1
 /* gett(id: string, index: number) {
    console.log('ID: ' + id);
    console.log('INDEX: ' + index);
    var res = id.split("/");
    console.log('date: ' + res[2] + ' name: ' + res[3]);
    this.fileService.getFile(res[2], res[3]).subscribe((data: Files) => {
      console.log('get Service ok');
      this.newFilesRes = data;
      let evilResponseProps = Object.keys(this.newFilesRes);
      for (let prop of evilResponseProps) {
        this.newgoodResponse.push(this.newFilesRes[prop]);
      }
      console.log(this.newgoodResponse[0]);
      let file = new Blob([this.newgoodResponse[0].getFile], { type: 'application/pdf' });            
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, error => {
      this.errorserver = true;
      console.log('Error', error);
    });
  }*/

  // Get 2
  get(id: string, index: number) {
    console.log('ID: ' + id);
    console.log('INDEX: ' + index);
    var res = id.split("/");
    console.log('date: ' + res[2] + ' name: ' + res[3]);
    this.fileService.getFile(res[2], res[3]).subscribe((data) => {
      console.log('get Service ok');
      var file = new Blob([data], { type: 'application/pdf' });            
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    }, error => {
      this.errorserver = true;
      console.log('Error', error);
    });
  }

  resetAlert(){
    this.errorserver = false;
    this.notfoundserver = false;
  }

  clearTable(){
    this.goodResponse = [];
  }

}
