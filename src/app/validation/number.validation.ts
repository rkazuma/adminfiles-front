import { AbstractControl, Validators } from '@angular/forms';

export function ValidateNumber(control: AbstractControl) {
  const min = 1;
  const max = 9999999999;
    const val: number = control.value;

    if (isNaN(val) || /\D/.test(val.toString())) {
      return {"number": true};
    } else if (!isNaN(1) && !isNaN(max)) {

      return val < min || val > max ? {"number": true} : null;
    } else if(!isNaN(min)) {

      return val < min ? {"number": true} : null;
    } else if(!isNaN(max)) {

      return val > max ? {"number": true} : null;
    } else {
      return null;
    }
  }
