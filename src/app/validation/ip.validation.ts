import { AbstractControl } from '@angular/forms';

export function ValidateIp(control: AbstractControl) {
  const validation = /^((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$/.test(control.value);
  if (!validation) {
    return { validIp: true };
  }
  return null;
}
// https://regex101.com/r/oH0bU6/5
