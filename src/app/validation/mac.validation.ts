import { AbstractControl } from '@angular/forms';

export function ValidateMac(control: AbstractControl) {
  const validation = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/.test(control.value);
  if (!validation) {
    return { validIp: true };
  }
  return null;
}
// https://regex101.com/r/oH0bU6/5
