import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

declare const module: { id: string };
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('routerAnimations', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class AppComponent implements OnInit {
  title = 'admfiles-front';

  ngOnInit(): void {
    // $(document).ready(function() { $('body').bootstrapMaterialDesign(); });
  }
  updateStatus(event: any) {
    console.log(event);
  }
  prepareRouteTransition(outlet) {
    const animation = outlet.activatedRouteData['animation'] || {};
    return animation['value'] || null;
  }
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    const exit = confirm('Do you want to leave this window?');
    if (exit === true) {
       // do something before closing;
    }
  }
}
