import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { MainService } from './main.service';
import { Files } from 'src/app/commons/models/files.model';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};


@Injectable()
export class FileService extends MainService {
    constructor(private http: HttpClient) {
        super();
    }

    health() {
        return this.http.get(this.urlHealth);
    }

    getFiles(): Observable<Files> {
        console.log('ask someone if this work service');
        return this.http.get<Files>(this.urlList).pipe(
            retry(1),
            catchError(this.handleError)
        );
    }

    getFileByDate(date: string): Observable<Files> {
        console.log('ask someone if getFileByDate service');
        let params = new HttpParams().set("date", date);
        return this.http.get<Files>(this.urlByDate, { params: params }).pipe(
            retry(1),
            catchError(this.handleError)
        );
    }

    getFileById(id: string): Observable<Files> {
        console.log('ask someone if getFileById service');
        let params = new HttpParams().set("id", id);
        return this.http.get<Files>(this.urlByName, { params: params }).pipe(
            retry(1),
            catchError(this.handleError)
        );
    }

    getFile(datefile:string ,nameId: string) {
        console.log('ask someone if getFile service');
        let params = new HttpParams().set('id', nameId).set('date', datefile);
        const httpOptions = {'responseType'  : 'arraybuffer' as 'json'};
        let headers = new HttpHeaders().set('Accept', 'application/pdf');
        return this.http.get(this.urlFile, { headers: headers, responseType: 'blob', params: params }).pipe(
            retry(1),
            catchError(this.handleError)
        );
    }




    // Error handling 
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }
}