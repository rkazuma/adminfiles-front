import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { MainService } from './main.service';

const httpOptions = {
  headers: new HttpHeaders({
    'contentType': 'application/json'
  })
};

@Injectable()
export class AuthenticationService extends MainService {
  constructor(private http: HttpClient) {
    super();
  }

  login(tiket: string) {
    this.uri = 'login';
    return this.http.post<any>(this.urlBase + this.uri, { ticket: tiket }, httpOptions)
      .pipe(
        tap(
          data => {
            if (data && data.token) {
              localStorage.setItem('currentUser', JSON.stringify(data));
            }
            return data;
          },
          error => error
        )
      );
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
