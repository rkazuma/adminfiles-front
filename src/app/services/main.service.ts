import { environment } from 'src/environments/environment';

export abstract class MainService {
    urlBase: string;
    urlApi: string;
    urlByDate: string;
    urlByName: string;
    urlFile: string;
    urlList: string;
    urlHealth: string;
    uri: string;
    constructor() {
        this.urlBase = environment.base_url;
        //this.urlBase = 'http://localhost:8080/Adminfiles/';
        //this.urlBase = 'https://150.250.220.53/JDS1PVAP3/Adminfiles/';
        this.urlList = this.urlBase + 'adm-files/getlist';
        this.urlByDate = this.urlBase + 'adm-files/getlistbydate';
        this.urlByName = this.urlBase + 'adm-files/getfilebyname';
        this.urlFile = this.urlBase + 'adm-files/getfile';
        this.urlHealth = this.urlBase + 'adm-files/health';
    }
}
