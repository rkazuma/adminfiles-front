import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Params } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private auth: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
        const param = route.queryParams['tiket'];
        if (param) {
          this.auth.login(param).subscribe((data) => {
            if (data && data.token) {
              return true;
            } else {
              this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
              return false;
            }
          },
          (error) => {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
            return false;
          });
        } else {
          this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
          return false;
        }
    }
}
