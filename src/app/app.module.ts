import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';

import { FilesComponent } from './files/files.component';
import { FileService } from './services/files.service';

import { AuthGuard } from './guard/authguard';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './services/authentication.service';
import { JwtInterceptor } from './interceptor/jwtinterceptor';

@NgModule({
  declarations: [
    AppComponent,
    FilesComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: FilesComponent,
        canActivate: [AuthGuard]
      }, {
        path: 'files',
        component: FilesComponent,
        canActivate: [AuthGuard]
      }, {
        path: 'login',
        component: LoginComponent
      }])
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: window['_app_base'] || '/' },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AuthGuard, AuthenticationService, FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
